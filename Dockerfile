FROM ubuntu:14.04

MAINTAINER GULL <gull@163.com>
ADD sources.list /etc/apt/sources.list


ADD software/mysql-connector-java-8.0.26.jar /tmp
ADD software/commons-lang-2.6.jar /tmp
ADD software/sqoop-1.4.7.tar.gz /usr/local

COPY config/* /tmp/

WORKDIR /root

# 安装软件
RUN apt-get update && \
    apt-get install -y openssh-server  && \
    apt-get install -y wget  && \
    apt-get clean all && \
    wget --no-check-certificate https://mirrors.tuna.tsinghua.edu.cn/apache/hadoop/common/hadoop-3.2.3/hadoop-3.2.3.tar.gz  && \
    wget --no-check-certificate https://mirrors.tuna.tsinghua.edu.cn/Adoptium/8/jdk/x64/linux/OpenJDK8U-jdk_x64_linux_hotspot_8u332b09.tar.gz && \
    wget --no-check-certificate https://mirrors.tuna.tsinghua.edu.cn/apache/hive/hive-3.1.2/apache-hive-3.1.2-bin.tar.gz && \
    tar -xzvf OpenJDK8U-jdk_x64_linux_hotspot_8u332b09.tar.gz -C /usr/local/ && \
    tar -xzvf hadoop-3.2.3.tar.gz -C /usr/local/  && \
    tar -xzvf apache-hive-3.1.2-bin.tar.gz -C /usr/local/  && \
    mv /usr/local/jdk8u332-b09 /usr/local/jdk && \
    mv /usr/local/hadoop-3.2.3 /usr/local/hadoop && \
    mv /usr/local/apache-hive-3.1.2-bin /usr/local/hive && \
    mv /tmp/mysql-connector-java-8.0.26.jar /usr/local/hive/lib/mysql-connector-java-8.0.26.jar && \
    mv /usr/local/sqoop-1.4.7 /usr/local/sqoop && \
    mv /tmp/commons-lang-2.6.jar /usr/local/sqoop/lib && \
    mv /usr/local/hive/lib/mysql-connector-java-8.0.26.jar /usr/local/sqoop/lib



# set environment variable
ENV JAVA_HOME=/usr/local/jdk
ENV HADOOP_HOME=/usr/local/hadoop
ENV HIVE_HOME=/usr/local/hive
ENV SQOOP_HOME=/usr/local/sqoop
ENV PATH=$PATH:$JAVA_HOME/bin:$HADOOP_HOME/bin:$HADOOP_HOME/sbin:$HIVE_HOME/bin:$SQOOP_HOME/bin

# ssh without key and hadoop config
RUN ssh-keygen -t rsa -f ~/.ssh/id_rsa -P '' && \
    cat ~/.ssh/id_rsa.pub >> ~/.ssh/authorized_keys && \
    mkdir -p ~/hdfs/namenode && \ 
    mkdir -p ~/hdfs/datanode && \
    mkdir $HADOOP_HOME/logs && \
    mv /tmp/ssh_config ~/.ssh/config && \
    mv /tmp/hadoop-env.sh $HADOOP_HOME/etc/hadoop/hadoop-env.sh && \
    mv /tmp/hdfs-site.xml $HADOOP_HOME/etc/hadoop/hdfs-site.xml && \ 
    mv /tmp/core-site.xml $HADOOP_HOME/etc/hadoop/core-site.xml && \
    mv /tmp/mapred-site.xml $HADOOP_HOME/etc/hadoop/mapred-site.xml && \
    mv /tmp/yarn-site.xml $HADOOP_HOME/etc/hadoop/yarn-site.xml && \
    mv /tmp/workers $HADOOP_HOME/etc/hadoop/workers && \
    mv /tmp/myhadoop.sh ~/myhadoop.sh && \
    mv /tmp/run-wordcount.sh ~/run-wordcount.sh && \
    mv /tmp/hive-site.xml /usr/local/hive/conf/ && \
    mv /tmp/sqoop-env.sh /usr/local/sqoop/conf/ && \
    chmod +x ~/myhadoop.sh && \
    chmod 600 /root/.ssh/config && \
    chmod +x ~/run-wordcount.sh && \
    chmod +x $HADOOP_HOME/sbin/start-dfs.sh && \
    chmod +x $HADOOP_HOME/sbin/start-yarn.sh && \
    rm -rf  /usr/local/hive/lib/guava-19.0.jar && \
    cp /usr/local/hadoop/share/hadoop/common/lib/guava-27.0-jre.jar /usr/local/hive/lib/guava-27.0-jre.jar && \
    rm -rf /usr/local/hive/lib/log4j-slf4j-impl-2.10.0.jar && \
    rm -rf /root/OpenJDK8U-jdk_x64_linux_hotspot_8u332b09.tar.gz   && \
    rm -rf /root/apache-hive-3.1.2-bin.tar.gz  && \
    rm -rf /root/hadoop-3.2.3.tar.gz && \
    $HADOOP_HOME/bin/hdfs namenode -format 
 

#不能在创建镜像的时候初始化hive，需要在容器内手动执行。
#$HIVE_HOME/bin/schematool -initSchema -dbType mysql 
CMD [ "sh", "-c", "service ssh start; bash"]

