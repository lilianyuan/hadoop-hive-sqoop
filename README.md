#此镜像为hadoop3，hive，sqoop的镜像，

#需要搭配一个 172.18.0.201的mysql数据库存储hive的元数据。

#config文件解读

##1、hadoop-env.sh 修改了javahome路径，为真实路径
##2、hive-site.xml 修改了mysql路径，为 真实路径
##3、myhadoop.sh hadoop启动脚本



#操作步骤

    chmod +x ./build-image.sh

    ./start-container.sh

#含义

##1、运行build-image.sh  即，创建一个名为gull/hadoop-hive:1.0镜像

##2、创建hadoop网络，如果创建，就不用创建了

    sudo docker network create --driver=bridge  --subnet=172.18.0.0/16 --gateway=172.18.0.1   hadoop

创建mysql数据库，如果创建过就不用创建

    docker run -itd --name mysql --hostname mysql --net hadoop --ip 172.18.0.201 -e MYSQL_ROOT_PASSWORD=root -p3306:3306  mysql:5.7




##3、运行start-container.sh 即，利用该镜像创建三个服务器，一主二从

##4、运行成功后会进入主节点

##5、运行 myhadoop start /status /stop 查看运行效果。

##6、exit退出容器

注意，如果曾经自己用windows编写了脚本，放在centos运行可能会失败

因为windows记事本默认的格式为dos格式

脚本需要的是unix格式，

需要改格式

在centos里用

sed -i "s/\r//" filename 可行。