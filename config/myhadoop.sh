#!/bin/bash
if [$# !=1 ];then

  echo "NoArgsInput...start,stop or status "
  exit 1;

fi

case $1 in

"start")
        echo "===================启动hadoop集群======================"
        $HADOOP_HOME/sbin/start-dfs.sh
        echo -e "\n"
        $HADOOP_HOME/sbin/start-yarn.sh
        echo -e "\n"
;;
"stop")
        echo "===================关闭hadoop集群======================"
        $HADOOP_HOME/sbin/stop-yarn.sh
        echo -e "\n"
        $HADOOP_HOME/sbin/stop-dfs.sh
        echo -e "\n"
;;
"status")
        echo"===================查看hadoop集群======================"
        for host in hadoop-master hadoop-slave1 hadoop-slave2
        do
                echo "-----------------hadoop集群$host-------------------"
                ssh $host "$JAVA_HOME/bin/jps"
        done
esac

