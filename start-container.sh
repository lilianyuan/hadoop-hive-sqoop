#!/bin/bash

#创建网络

sudo docker network create --driver=bridge  --subnet=172.18.0.0/16 --gateway=172.18.0.1   hadoop


# the default node number is 3
N=${1:-3}


# start hadoop master container
sudo docker rm -f hadoop-master &> /dev/null
echo "start hadoop-master container..."
sudo docker run -itd \
                --net hadoop \
		--ip 172.18.0.2 \
                --name hadoop-master \
                --hostname hadoop-master \
		-p9870:9870 \
		-p8088:8088 \
                gull/hadoop-hive:1.0 &> /dev/null


# start hadoop slave container
i=1
j=3
while [ $i -lt $N ]
do
	sudo docker rm -f hadoop-slave$i &> /dev/null
	echo "start hadoop-slave$i container..."
	sudo docker run -itd \
	                --net hadoop \
			--ip 172.18.0.$j \
	                --name hadoop-slave$i \
	                --hostname hadoop-slave$i \
	                gull/hadoop-hive:1.0 &> /dev/null
	i=$(( $i + 1 ))
	j=$(( $j + 1 ))
done 

# get into hadoop master container
sudo docker exec -it hadoop-master bash

